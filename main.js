export function setup(ctx) {
    const id = 'semi-auto-eat';
    const title = 'SEMI Auto Eat';

    // setting groups
    const SETTING_GENERAL = 'General';

    const currentFood = () => game.combat.player.food.slots[game.combat.player.food.selectedSlot];
    const currentFoodHealing = () => game.combat.player.getFoodHealing(currentFood().item);
    const currentFoodQty = () => currentFood().quantity;

    let didHeal = false; //Tracks whether or not we ate before an attack in an incredibly kludgy way

    const beforeDamage = (amount, source, thieving) => {
        // Augment the damage amount for burns
        if (source === 'Burn' && game.combat.enemy.target.modifiers.increasedMaxHPBurnDamage > 1) {
            amount += Math.floor(game.combat.player.stats.maxHitpoints * (game.combat.player.target.modifiers.increasedMaxHPBurnDamage / 100));
        }

        // How much health might we need to heal?
        const playerMaxHP = game.combat.player.stats.maxHitpoints;
        const playerCurrentHP = game.combat.player.hitpoints;
        const playerHPtoMax = playerMaxHP - playerCurrentHP;

        // How much do we need to heal?
        const hpDeficit = amount - playerCurrentHP;

        // Do we even need to heal?
        if(hpDeficit < 0) {
            return;
        }

        // This hit cannot be healed to prevent. Exit combat.
        if(amount > playerMaxHP) {
            mod.api.SEMI.log(id, 'Enemy next hit greater than player max HP. Embrace death my friend.');
        }

        // Eats meats
        if (!thieving) { //We don't heal to full before damage unless we need to to survive.
            var foodQty = Math.floor(playerHPtoMax / currentFoodHealing());
            if (foodQty*currentFoodHealing() < hpDeficit){ //Wasting food is needed to survive, do it anyway.
                foodQty++;
            }
        } else { //We're taking damage from thieving, so heal just enough to survive.
                 //Manual eating costs nothing when thieving so we can just do it again when needed.
            var foodQty = Math.ceil(hpDeficit / currentFoodHealing());
        }
        foodQty = Math.min(currentFoodQty(),foodQty);
        game.combat.player.eatFood(foodQty, true);

        // Report results
        mod.api.SEMI.log(id, `Manually ate ${foodQty} ${currentFood().item.name} before taking damage.`);

        didHeal = true;

        // Handle still not having enough HP after eating.
        if(amount >= game.combat.player.hitpoints) {
            const canSwapFood = !game.combat.player.checkIfCantEquip();
            if (canSwapFood){
                const nonEmptySlot = game.combat.player.food.slots.findIndex((slot) => slot.item !== game.emptyFoodItem); //yoink'd from player.autoEat
                if (nonEmptySlot >= 0) {
                    game.combat.player.food.setSlot(nonEmptySlot); //Use setSlot to avoid the renderFood from selectFood
                    mod.api.SEMI.log(id, 'Automatically swapped food to slot '+nonEmptySlot);
                    beforeDamage(amount, source, thieving);
                } else {
                    mod.api.SEMI.log(id, 'Not enough HP to survive next attack even after eating, and there\'s no food to swap to.');
                }
            } else {
                mod.api.SEMI.log(id, 'Not enough HP to survive next attack even after eating, and can\'t swap food.');
            }
        }
    }

    const afterDamage = () => {
        if (!didHeal){ //If we didn't heal before this attack, then don't do so after so as not to interrupt attacks.
            return;
        }
        // How much health might we need to heal?
        const playerMaxHP = game.combat.player.stats.maxHitpoints;
        const playerCurrentHP = game.combat.player.hitpoints;
        const playerHPtoMax = playerMaxHP - playerCurrentHP;

        const wasteFood = ctx.settings.section(SETTING_GENERAL).get(`${id}-waste-food`);
        if (wasteFood){ //Heal to full even if it wastes food.
            var foodQty = Math.ceil(playerHPtoMax / currentFoodHealing());
        } else { //Don't waste food.
            var foodQty = Math.floor(playerHPtoMax / currentFoodHealing());
        }
        foodQty = Math.min(currentFoodQty(),foodQty);
        game.combat.player.eatFood(foodQty, true);

        // Report results
        mod.api.SEMI.log(id, `Manually ate ${foodQty} ${currentFood().item.name} after taking damage.`);
    }

    ctx.patch(Player, 'damage').before((amount, source, thieving) => {
        if (ctx.settings.section(SETTING_GENERAL).get(`${id}-enable`)){
            //Heal to survive the hit.
            beforeDamage(amount, source, thieving);
        }
    });

    ctx.patch(Player, 'damage').after((rval, amount, source, thieving) => {
        if (ctx.settings.section(SETTING_GENERAL).get(`${id}-enable`) && !thieving){
            //Heal to full/near-full after taking damage in combat.
            //This means we can take more damage before needing to eat again.
            //Thieving has no penalty for eating so it's most efficient to not do this there.
            afterDamage();
        }
        didHeal = false; //Reset this here
    });

    ctx.settings.section(SETTING_GENERAL).add({
        'type': 'switch',
        'name': `${id}-enable`,
        'label': `Enable ${title}`,
        'default': true
    });

    ctx.settings.section(SETTING_GENERAL).add({
        'type': 'switch',
        'name': `${id}-waste-food`,
        'label': `Waste food for full heal`,
        'hint': 'Heals to full HP in Combat, even if this wastes healing value. Less efficient food use, but you don\'t need to eat as often. Default: on',
        'default': true
    });



    mod.api.SEMI.log(id, "Successfully loaded");
}
